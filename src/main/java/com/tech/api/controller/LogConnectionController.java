package com.tech.api.controller;

import com.tech.api.model.LogConnections;
import com.tech.api.service.LogConnectionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})

public class LogConnectionController {
    @Autowired
    private LogConnectionsService logConnectionsService;

    @GetMapping("/getLoginLogs")
    public List<LogConnections> getLogs(){
        return logConnectionsService.findAll();
    }

    @GetMapping("/searchLogin/{busqueda}")
    public List<LogConnections> buscar(@PathVariable String busqueda){
        return logConnectionsService.buscar(busqueda);
    }
}
