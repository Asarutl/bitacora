package com.tech.api.controller;

import com.tech.api.model.LogConnections;
import com.tech.api.model.Product;
import com.tech.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/findAllProducts")
    public List<Product> findAllProducts(){
        return productService.getProducts();
    }

    @PostMapping("/addProduct")
    public Product saveProduct(@RequestBody Product product){
        return productService.saveProduct(product);
    }
    @PutMapping("/updateProduct")
    public Product updateProduct(@RequestBody Product product){
        return productService.updateProduct(product);
    }
    @DeleteMapping("/deleteProduct/{id}")
    public boolean deleteProduct(@PathVariable Long id){
        return productService.deleteProduct(id);
    }
    
    @GetMapping("/findById/{id}")
    public Product getProduct(@PathVariable Long id){
        return productService.findById(id);
    }
}
