package com.tech.api.controller;

import com.tech.api.model.ChangeLog;
import com.tech.api.service.ChangeLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})

public class ChangeLogController {
    @Autowired
    private ChangeLogService changeLogService;

    @GetMapping("/getChangeLogs")
    public List<ChangeLog> getLogs(){
        return changeLogService.getChangeLog();
    }

    @GetMapping("/search/{busqueda}")
    public List<ChangeLog> buscar(@PathVariable String busqueda){
        return changeLogService.buscar(busqueda);
    }
}
