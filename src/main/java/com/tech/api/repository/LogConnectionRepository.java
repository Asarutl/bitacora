package com.tech.api.repository;

import com.tech.api.model.LogConnections;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogConnectionRepository extends JpaRepository<LogConnections, Long> {
	
	public List<LogConnections> findAllByOrderByAttemptDateDesc();
	
}
