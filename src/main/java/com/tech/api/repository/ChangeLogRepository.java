package com.tech.api.repository;

import com.tech.api.model.ChangeLog;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChangeLogRepository extends JpaRepository<ChangeLog, Long> {
	public List<ChangeLog> findAllByOrderByChangeDateDesc();
}
