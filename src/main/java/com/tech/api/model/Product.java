package com.tech.api.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="product")
public class Product implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private double price;
    private double quantity;

    @ManyToOne(
            fetch = FetchType.EAGER,
            optional = false
    )
    private LogConnections user;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public LogConnections getUser() {
        return user;
    }

    public void setUser(LogConnections user) {
        this.user = user;
    }
}
