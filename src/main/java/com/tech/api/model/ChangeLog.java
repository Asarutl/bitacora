package com.tech.api.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "changelog")
public class ChangeLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String changeDate;
    private String sqlQuery;
    private String newInformation;
    private String oldInformation;
    @ManyToOne(
            fetch = FetchType.EAGER,
            optional = false
    )
    private LogConnections user;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(String changeDate) {
        this.changeDate = changeDate;
    }

    public String getSqlQuery() {
        return sqlQuery;
    }

    public void setSqlQuery(String sqlQuery) {
        this.sqlQuery = sqlQuery;
    }

    public String getNewInformation() {
        return newInformation;
    }

    public void setNewInformation(String newInformation) {
        this.newInformation = newInformation;
    }

    public String getOldInformation() {
        return oldInformation;
    }

    public void setOldInformation(String oldInformation) {
        this.oldInformation = oldInformation;
    }

    public LogConnections getUser() {
        return user;
    }

    public void setUser(LogConnections user) {
        this.user = user;
    }
}
