package com.tech.api.service;

import com.tech.api.model.ChangeLog;
import com.tech.api.repository.ChangeLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class ChangeLogService {

    @Autowired
    private ChangeLogRepository changeLogRepository;

    @Autowired
    EntityManager entityManager;

    public List<ChangeLog> getChangeLog(){
        return changeLogRepository.findAllByOrderByChangeDateDesc();
    }


    public List<ChangeLog> buscar(String busqueda){
        return new ArrayList<>();
    }
}
