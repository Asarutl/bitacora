package com.tech.api.service;

import com.tech.api.model.LogConnections;
import com.tech.api.model.User;
import com.tech.api.repository.LogConnectionRepository;
import com.tech.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    LogConnectionRepository logConnectionRepository;

    public List<User> findAll(){
        return userRepository.findAll();
    }
    public User saveUser(User user){
        if (user!=null){
            return userRepository.save(user);
        }else {
            return new User();
        }
    }

    public User updateUser(User user){
        if (userRepository.findById(user.getId()).isPresent()){
            return userRepository.save(user);
        }else{
            return new User();
        }
    }

    public boolean deleteUser(Long id){
        if (userRepository.findById(id).isPresent()){
            userRepository.deleteById(id);
            return true;
        }else {
            return false;
        }
    }

    public LogConnections login(LogConnections data){
        User user = userRepository.findByEmailAndPassword(data.getUser().getEmail(), data.getUser().getPassword());
        LogConnections logConnection = new LogConnections();
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        if (user!=null){
            logConnection.setAttemptDate(dtf.format(now));
            logConnection.setUser(user);
            logConnection.setHostname(data.getHostname());
            logConnection = logConnectionRepository.save(logConnection);
            return logConnection;
        }else {
            return new LogConnections();
        }
    }
}
