package com.tech.api.service;

import com.tech.api.model.LogConnections;
import com.tech.api.model.Product;
import com.tech.api.repository.LogConnectionRepository;
import com.tech.api.repository.ProductRepository;
import com.tech.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
     @Autowired
     private ProductRepository productRepository;

     @Autowired
     private LogConnectionRepository logConnectionRepository;

     //--Find all
     public List<Product> getProducts(){
         return productRepository.findAll();
     }

     //--Add
     public Product saveProduct(Product product){
         LogConnections user = logConnectionRepository.findById(product.getUser().getId()).get();
         product.setUser(user);
         return productRepository.save(product);
     }
     //--Update
     public Product updateProduct(Product product){
         if (productRepository.findById(product.getId()).isPresent()){
             LogConnections user = logConnectionRepository.findById(product.getUser().getId()).get();
             product.setUser(user);
             return productRepository.save(product);
         }else{
             return new Product();
         }
     }
     //--Delete
     public boolean deleteProduct(Long id){
         if (productRepository.findById(id).isPresent()){
             productRepository.deleteById(id);
             return true;
         }else {
             return false;
         }
     }
     
     public Product findById(Long id) {
    	 return productRepository.findById(id).get();
     }
}
