package com.tech.api.service;

import com.tech.api.model.ChangeLog;
import com.tech.api.model.LogConnections;
import com.tech.api.repository.LogConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class LogConnectionsService {

    @Autowired
    private LogConnectionRepository logConnectionRepository;

    @Autowired
    EntityManager entityManager;

    public List<LogConnections> findAll(){
        return logConnectionRepository.findAllByOrderByAttemptDateDesc();
    }

    public List<LogConnections> buscar(String busqueda){
        return new ArrayList<>();
    }
}
